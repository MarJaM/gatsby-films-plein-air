# Festival des sites de plein air

## Site réalisé dans le cadre d'un exercice Openclassrooms

Ce site est réalisé avec Gatsby et Netlify CMS Starter.

Pour voir le site en local, lancer les commandes suivantes puis visiter http://localhost:8000 

```bash
cd [DOSSIER_DU_SITE]
npm install
npm run start
```