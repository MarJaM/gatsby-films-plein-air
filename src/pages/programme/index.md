---
templateKey: programme-page
title: Programme
description: Consultez le programme du Festival des Films de Plein Air
programme:
  jours:
    - date: 5 août
      films:
        - acteurs:
            - Roman Bilyk
            - Irina Starshenbaum
            - Yoo Teo
          annee: '2019'
          duree: 126
          image: /img/leto.png
          lien: 'https://www.club-vo.fr/films/leto/'
          realisateur: Kirill Serebrennikov
          seance: 18h00
          synopsys: >-
            Leningrad. Un été du début des années 80. En amont de la
            Perestroïka, les disques de Lou Reed et de David Bowie s’échangent
            en contrebande, et une scène rock émerge. Mike et sa femme la belle
            Natacha rencontrent le jeune Viktor Tsoï. Entourés d’une nouvelle
            génération de musiciens, ils vont changer le cours du rock’n’roll en
            Union Soviétique. 
          titre: Leto
        - acteurs:
            - Lily Franky
            - Sakura Andô
            - Mayu Matsuoka
          annee: '2018'
          duree: 121
          image: /img/une_affaire_de_famille.jpg
          lien: 'https://www.club-vo.fr/films/une-affaire-de-famille/'
          realisateur: ' Hirokazu Kore-eda'
          seance: 20h
          synopsys: >-
            Osamu travaille comme journalier. Lui et son fils Shota aiment voler
            à l'étalage. Sur le chemin de la maison, Osamu trouve un soir, après
            une session de vol, la petite Yuri qu'il ramène chez lui, où vivent
            déjà sa femme Nobuyo, sa belle-sœur Aki et la grand-mère Hatsue. Ils
            forment ensemble une famille pauvre mais heureuse.
          titre: Une affaire de famille
        - acteurs:
            - Alex R. Hibbert
            - Ashton Sanders
            - Trevante Rhodes
          annee: '2017'
          duree: 110
          image: /img/moonlight.jpg
          lien: 'https://player.vimeo.com/video/192774442'
          realisateur: Barry Jenkins
          seance: 22h
          synopsys: >-
            Dans le Miami des années 80 en proie à la drogue, Moonlight se
            concentre sur trois moments de la vie de Chiron, un jeune homme qui
            se bat contre une vie familiale rude et sa sexualité naissante.
          titre: Moonlight
    - date: 6 août
      films:
        - acteurs:
            - Roman Bilyk
            - Irina Starshenbaum
            - Yoo Teo
          annee: '2019'
          duree: 126
          image: /img/leto.png
          lien: 'https://www.club-vo.fr/films/leto/'
          realisateur: Kirill Serebrennikov
          seance: 18h00
          synopsys: >-
            Leningrad. Un été du début des années 80. En amont de la
            Perestroïka, les disques de Lou Reed et de David Bowie s’échangent
            en contrebande, et une scène rock émerge. Mike et sa femme la belle
            Natacha rencontrent le jeune Viktor Tsoï. Entourés d’une nouvelle
            génération de musiciens, ils vont changer le cours du rock’n’roll en
            Union Soviétique. 
          titre: Leto
        - acteurs:
            - Lily Franky
            - Sakura Andô
            - Mayu Matsuoka
          annee: '2018'
          duree: 121
          image: /img/une_affaire_de_famille.jpg
          lien: 'https://www.club-vo.fr/films/une-affaire-de-famille/'
          realisateur: ' Hirokazu Kore-eda'
          seance: 20h
          synopsys: >-
            Osamu travaille comme journalier. Lui et son fils Shota aiment voler
            à l'étalage. Sur le chemin de la maison, Osamu trouve un soir, après
            une session de vol, la petite Yuri qu'il ramène chez lui, où vivent
            déjà sa femme Nobuyo, sa belle-sœur Aki et la grand-mère Hatsue. Ils
            forment ensemble une famille pauvre mais heureuse.
          titre: Une affaire de famille
        - acteurs:
            - Alex R. Hibbert
            - Ashton Sanders
            - Trevante Rhodes
          annee: '2017'
          duree: 110
          image: /img/moonlight.jpg
          lien: 'https://player.vimeo.com/video/192774442'
          realisateur: Barry Jenkins
          seance: 22h
          synopsys: >-
            Dans le Miami des années 80 en proie à la drogue, Moonlight se
            concentre sur trois moments de la vie de Chiron, un jeune homme qui
            se bat contre une vie familiale rude et sa sexualité naissante.
          titre: Moonlight
    - date: 7 août
      films:
        - acteurs:
            - Roman Bilyk
            - Irina Starshenbaum
            - Yoo Teo
          annee: '2019'
          duree: 126
          image: /img/leto.png
          lien: 'https://www.club-vo.fr/films/leto/'
          realisateur: Kirill Serebrennikov
          seance: 18h00
          synopsys: >-
            Leningrad. Un été du début des années 80. En amont de la
            Perestroïka, les disques de Lou Reed et de David Bowie s’échangent
            en contrebande, et une scène rock émerge. Mike et sa femme la belle
            Natacha rencontrent le jeune Viktor Tsoï. Entourés d’une nouvelle
            génération de musiciens, ils vont changer le cours du rock’n’roll en
            Union Soviétique. 
          titre: Leto
        - acteurs:
            - Lily Franky
            - Sakura Andô
            - Mayu Matsuoka
          annee: '2018'
          duree: 121
          image: /img/une_affaire_de_famille.jpg
          lien: 'https://www.club-vo.fr/films/une-affaire-de-famille/'
          realisateur: ' Hirokazu Kore-eda'
          seance: 20h
          synopsys: >-
            Osamu travaille comme journalier. Lui et son fils Shota aiment voler
            à l'étalage. Sur le chemin de la maison, Osamu trouve un soir, après
            une session de vol, la petite Yuri qu'il ramène chez lui, où vivent
            déjà sa femme Nobuyo, sa belle-sœur Aki et la grand-mère Hatsue. Ils
            forment ensemble une famille pauvre mais heureuse.
          titre: Une affaire de famille
        - acteurs:
            - Alex R. Hibbert
            - Ashton Sanders
            - Trevante Rhodes
          annee: '2017'
          duree: 110
          image: /img/moonlight.jpg
          lien: 'https://player.vimeo.com/video/192774442'
          realisateur: Barry Jenkins
          seance: 22h
          synopsys: >-
            Dans le Miami des années 80 en proie à la drogue, Moonlight se
            concentre sur trois moments de la vie de Chiron, un jeune homme qui
            se bat contre une vie familiale rude et sa sexualité naissante.
          titre: Moonlight
    - date: 8 août
      films:
        - acteurs:
            - Roman Bilyk
            - Irina Starshenbaum
            - Yoo Teo
          annee: '2019'
          duree: 126
          image: /img/leto.png
          lien: 'https://www.club-vo.fr/films/leto/'
          realisateur: Kirill Serebrennikov
          seance: 18h00
          synopsys: >-
            Leningrad. Un été du début des années 80. En amont de la
            Perestroïka, les disques de Lou Reed et de David Bowie s’échangent
            en contrebande, et une scène rock émerge. Mike et sa femme la belle
            Natacha rencontrent le jeune Viktor Tsoï. Entourés d’une nouvelle
            génération de musiciens, ils vont changer le cours du rock’n’roll en
            Union Soviétique. 
          titre: Leto
        - acteurs:
            - Lily Franky
            - Sakura Andô
            - Mayu Matsuoka
          annee: '2018'
          duree: 121
          image: /img/une_affaire_de_famille.jpg
          lien: 'https://www.club-vo.fr/films/une-affaire-de-famille/'
          realisateur: ' Hirokazu Kore-eda'
          seance: 20h
          synopsys: >-
            Osamu travaille comme journalier. Lui et son fils Shota aiment voler
            à l'étalage. Sur le chemin de la maison, Osamu trouve un soir, après
            une session de vol, la petite Yuri qu'il ramène chez lui, où vivent
            déjà sa femme Nobuyo, sa belle-sœur Aki et la grand-mère Hatsue. Ils
            forment ensemble une famille pauvre mais heureuse.
          titre: Une affaire de famille
        - acteurs:
            - Alex R. Hibbert
            - Ashton Sanders
            - Trevante Rhodes
          annee: '2017'
          duree: 110
          image: /img/moonlight.jpg
          lien: 'https://player.vimeo.com/video/192774442'
          realisateur: Barry Jenkins
          seance: 22h
          synopsys: >-
            Dans le Miami des années 80 en proie à la drogue, Moonlight se
            concentre sur trois moments de la vie de Chiron, un jeune homme qui
            se bat contre une vie familiale rude et sa sexualité naissante.
          titre: Moonlight
---

