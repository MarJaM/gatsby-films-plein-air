import React from 'react'
import Layout from '../../components/Layout'

export default () => (
  <Layout>
    <section className="section" style={{ minHeight: '87vh' }}>
      <div className="container">
        <div className="content">
          <h1>Merci!</h1>
          <p>Votre message a bien été envoyé, on y répondra au plus vite.</p>
        </div>
      </div>
    </section>
  </Layout>
)
