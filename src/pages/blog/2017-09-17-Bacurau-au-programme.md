---
templateKey: blog-post
title: Le film Bacurau sera au programme du Festival
date: 2019-09-17T15:04:10.000Z
featuredpost: false
featuredimage: /img/bacurau.jpg
description: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque neque dolor, aliquam. 
tags:
  - movie
  - festival
---
![flavor wheel](/img/bacurau-2.jpg)

Morbi finibus erat non semper fermentum. Sed lorem tellus, sodales nec volutpat at, consectetur vel sem. Sed pharetra eleifend risus, eu fermentum elit fringilla id. Praesent ultrices vel tellus et porta. Mauris vulputate eleifend dolor ut euismod. In a venenatis neque. In non auctor sem. Donec consequat magna erat, quis commodo felis mattis a. Etiam id aliquet enim, in cursus diam. Proin ac finibus enim, ut mattis orci. Phasellus mattis tortor in nisl tempus, ut malesuada orci finibus. Nulla dapibus sed nisi in consequat.

## In a venenatis neque.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eu ultricies orci, a efficitur velit. Aenean blandit metus magna, quis cursus magna suscipit et. Duis et commodo ex. Donec purus lorem, molestie sit amet lacus a, dignissim sollicitudin tellus. Curabitur quis sollicitudin turpis, nec tincidunt lorem. Nulla facilisi. Morbi cursus euismod velit, vel consequat erat pellentesque ac. Nullam consequat mauris tellus, nec sagittis elit sagittis eu. Sed id mauris rutrum, convallis felis a, varius enim. Curabitur lacinia hendrerit nisi nec dictum. Nulla facilisi. Pellentesque augue tellus, facilisis ac consectetur id, sollicitudin ut ex. Quisque dapibus vitae ex nec laoreet. Ut eu sem eget ante dictum semper sit amet mattis quam. Interdum et malesuada fames ac ante ipsum primis in faucibus.

Morbi finibus erat non semper fermentum. Sed lorem tellus, sodales nec volutpat at, consectetur vel sem. Sed pharetra eleifend risus, eu fermentum elit fringilla id. Praesent ultrices vel tellus et porta. Mauris vulputate eleifend dolor ut euismod. In a venenatis neque. In non auctor sem. Donec consequat magna erat, quis commodo felis mattis a. Etiam id aliquet enim, in cursus diam. Proin ac finibus enim, ut mattis orci. Phasellus mattis tortor in nisl tempus, ut malesuada orci finibus. Nulla dapibus sed nisi in consequat. Cras sollicitudin purus sollicitudin mauris consequat interdum. Donec eu sapien eros. Morbi accumsan, arcu a maximus rutrum, magna augue viverra augue, in eleifend massa sem nec turpis. 
