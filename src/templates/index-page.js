import React from 'react'
import PropTypes from 'prop-types'
import { Link, graphql } from 'gatsby'
import ModalButton from '../components/ModalButton'
import Layout from '../components/Layout'
import BlogRoll from '../components/BlogRoll'

export const IndexPageTemplate = ({ image, title, subheading }) => (
  <div>
    <div
      className="full-width-image"
      style={{
        backgroundImage: `url(${
          !!image.childImageSharp ? image.childImageSharp.fluid.src : image
        })`,
      }}
    >
      <div
        style={{
          display: 'flex',
          minHeight: '330px',
          lineHeight: '1',
          justifyContent: 'space-around',
          alignItems: 'left',
          flexDirection: 'column',
          backgroundColor: 'rgba(51, 51, 51, 0.5)',
          padding: '10px',
        }}
      >
        <h1
          className="has-text-weight-bold is-size-3-mobile is-size-2-tablet is-size-1-widescreen has-text-centered"
          style={{
            borderLeft: '0.5rem solid rgb(255, 68, 0)',
            borderRight: '0.5rem solid rgb(255, 68, 0)',
            //backgroundColor: 'rgb(255, 68, 0)',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
          }}
        >
          {title}
        </h1>
        <h2
          className="has-text-weight-bold has-text-centered is-size-5-mobile is-size-5-tablet is-size-4-widescreen"
          style={{
            borderLeft: '0.5rem solid rgb(255, 68, 0)',
            borderRight: '0.5rem solid rgb(255, 68, 0)',
            //backgroundColor: 'rgb(255, 68, 0)',
            color: 'white',
            lineHeight: '1',
            padding: '0.25em',
          }}
        >
          {subheading}
        </h2>
        <div className="columns below-title content is-centered is-vcentered">
          <div className="column is-3 is-12-mobile has-text-centered">
            <span className="tag is-info is-medium has-text-centered">
              Parc Monceau
            </span>
          </div>
          <div className="column is-3 is-12-mobile has-text-centered">
            <span className="tag is-info is-medium">
              Du 5 au 8 août
            </span>
          </div>
          <div className="column is-3 is-12-mobile has-text-centered">
            <span className="tag is-info is-medium">
              De 18h à minuit
            </span>
          </div>
          <div className="column is-3 is-12-mobile has-text-centered">
            <span className="tag is-info is-medium">
              Accès gratuit
            </span>
          </div>
        </div>
        <div className="columns is-centered is-marginless">
          <div className="column is-4 has-text-centered">
            <Link
              className="button is-large is-primary is-rounded is-outlined is-inverted has-text-weight-bold"
              to="/programme"
            >
              Programme
            </Link>
          </div>
          <div className="column is-4 is-offset-2 has-text-centered">
            <ModalButton />
          </div>
        </div>
      </div>
    </div>
    <section className="section section--gradient">
      <div className="columns is-centered">
        <div className="column is-11">
          <h2 className="content has-text-weight-semibold is-size-2">
            Dernières actualités du festival
          </h2>
          <BlogRoll />
          <div className="column is-12 has-text-centered">
            <Link className="btn" to="/blog">
              Voir plus
            </Link>
          </div>
        </div>
      </div>
    </section>
  </div>
)

IndexPageTemplate.propTypes = {
  image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
  title: PropTypes.string,
  subheading: PropTypes.string,
}

const IndexPage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <IndexPageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        subheading={frontmatter.subheading}
      />
    </Layout>
  )
}

IndexPage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default IndexPage

export const pageQuery = graphql`
  query IndexPageTemplate {
    markdownRemark(frontmatter: { templateKey: { eq: "index-page" } }) {
      frontmatter {
        title
        image {
          childImageSharp {
            fluid(maxWidth: 2048, quality: 100) {
              ...GatsbyImageSharpFluid
            }
          }
        }
        subheading
      }
    }
  }
`
