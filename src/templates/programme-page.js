import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import Features from '../components/Features'

export const ProgrammePageTemplate = ({ title, description, programme }) => (
  <div className="content programme">
    <div>
      <h1
        className="has-text-weight-bold is-size-1"
        style={{
          boxShadow: '0.5rem 0 0 #f40, -0.5rem 0 0 #f40',
          backgroundColor: '#f40',
          color: 'white',
          padding: '1rem',
        }}
      >
        {title}
      </h1>
    </div>

    {programme.map(jour => (
      <section className="section jour-programme">
        <h2>{jour.date}</h2>
        <div className="columns">
          <div className="column is-10 is-offset-1">
            <Features gridItems={jour.films} />
          </div>
        </div>
      </section>
    ))}
  </div>
)

ProgrammePageTemplate.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  programme: PropTypes.shape({
    jours: PropTypes.array,
  }),
}

const ProgrammePage = ({ data }) => {
  const { frontmatter } = data.markdownRemark

  return (
    <Layout>
      <ProgrammePageTemplate
        image={frontmatter.image}
        title={frontmatter.title}
        programme={frontmatter.programme.jours}
      />
    </Layout>
  )
}

ProgrammePage.propTypes = {
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      frontmatter: PropTypes.object,
    }),
  }),
}

export default ProgrammePage

export const programmePageQuery = graphql`
  query ProgrammePage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      frontmatter {
        title
        description
        programme {
          jours {
            date
            films {
              acteurs
              annee
              duree
              image {
                childImageSharp {
                  fluid(maxWidth: 400, quality: 64) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
              lien
              realisateur
              seance
              synopsys
              titre
            }
          }
        }
      }
    }
  }
`
