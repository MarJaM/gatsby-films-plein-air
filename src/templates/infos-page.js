import React from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Layout from '../components/Layout'
import Content, { HTMLContent } from '../components/Content'
import ModalButton from '../components/ModalButton'

export const InfosPageTemplate = ({ title, content, contentComponent }) => {
  const PageContent = contentComponent || Content

  return (
    <div className="content">
      <div>
        <h1
          className="has-text-weight-bold is-size-1"
          style={{
            boxShadow: '0.5rem 0 0 #f40, -0.5rem 0 0 #f40',
            backgroundColor: '#f40',
            color: 'white',
            padding: '1rem',
          }}
        >
          {title}
        </h1>
      </div>
      <section className="section section--gradient">
        <section className="section">
          <div className="columns">
            <div className="column is-11 is-offset-1">
              <h2>Venir au festival</h2>
            </div>
          </div>
          <div className="columns">
            <div className="column is-4 is-offset-1">
              <div className="box has-text-centered">
                <h3>Réservation</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                {/*<div className="content has-text-centered">*/}
                <ModalButton />
                {/*</div>*/}
              </div>
              <h3>En vélo</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
              <h3>En métro</h3>
              <ul>
                <li>Risus feugiat in ante metus dictum at tempor commodo.</li>
                <li> Volutpat diam ut venenatis tellus in metus vulputate.</li>
              </ul>
              <h3>En bus</h3>
              <ul>
                <li>In hac habitasse platea dictumst vestibulum.</li>
                <li>Mattis pellentesque id nibh tortor.</li>
                <li>
                  Amet consectetur adipiscing elit duis tristique sollicitudin.
                </li>
              </ul>
            </div>
            <div className="column is-5 is-offset-1 map-container">
              <iframe
                title="Carte du Parc Monceau"
                width="600"
                height="500"
                frameborder="0"
                scrolling="no"
                marginheight="0"
                marginwidth="0"
                src="https://www.openstreetmap.org/export/embed.html?bbox=2.3070538043975835%2C48.87850037042901%2C2.3113909363746648%2C48.88017788359427&amp;layer=mapnik"
              ></iframe>
              <br />
              <small>
                <a href="https://www.openstreetmap.org/#map=18/48.87921/2.30818">
                  View Larger Map
                </a>
              </small>
            </div>
          </div>
        </section>
        <section className="section">
          <div className="columns">
            <div className="column is-10 is-offset-1">
              <PageContent className="content" content={content} />
            </div>
          </div>
        </section>
      </section>
    </div>
  )
}

InfosPageTemplate.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string,
  contentComponent: PropTypes.func,
}

const InfosPage = ({ data }) => {
  const { markdownRemark: post } = data

  return (
    <Layout>
      <InfosPageTemplate
        contentComponent={HTMLContent}
        title={post.frontmatter.title}
        content={post.html}
      />
    </Layout>
  )
}

InfosPage.propTypes = {
  data: PropTypes.object.isRequired,
}

export default InfosPage

export const infosPageQuery = graphql`
  query InfosPage($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        title
      }
    }
  }
`
