import React from 'react'

const ModalButton = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      modalActiveClass: '',
    }
  }

  toggleModal = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the modal accordingly
        this.state.active
          ? this.setState({
              modalActiveClass: 'is-active',
            })
          : this.setState({
              modalActiveClass: '',
            })
      }
    )
  }

  render() {
    const modal = (
      <div className={`modal ${this.state.modalActiveClass}`}>
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title">Formulaire de participation</p>
            <button
              className="delete"
              aria-label="close"
              onClick={this.toggleModal}
            ></button>
          </header>
          <section className="modal-card-body">
            <iframe
              title="Formulaire Inscription"
              scrolling="no"
              src="https://framaforms.org/festival-les-films-de-plein-air-1570893663"
              width="100%"
              height="85vh"
              border="0"
            ></iframe>
          </section>
        </div>
      </div>
    )

    return (
      <div>
        <button
          className="button is-large is-primary is-rounded has-text-weight-bold"
          onClick={this.toggleModal}
        >
          Je m'inscris
        </button>
        {modal}
      </div>
    )
  }
}

export default ModalButton
