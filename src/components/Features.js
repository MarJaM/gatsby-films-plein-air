import React from 'react'
import PropTypes from 'prop-types'
import PreviewCompatibleImage from '../components/PreviewCompatibleImage'

const FeatureGrid = ({ gridItems }) => (
  <div className="columns is-multiline">
    {gridItems.map(item => (
      <div key={item.titre} className="column is-4">
        <section className="card">
          <div className="card-image">
            <PreviewCompatibleImage imageInfo={item} />
          </div>
          <div className="card-content">
            <h3>{item.titre}</h3>
            <ul
              style={{ 'list-style-type': 'none' }}
              className="is-marginless is-size-7"
            >
              <li>
                <span className="caption-movie">Réalisé en</span> {item.annee}{' '}
                <span className="caption-movie">par</span> {item.realisateur}
              </li>
              <li>
                <span className="caption-movie">Avec</span>{' '}
                {item.acteurs.map((acteur, index) =>
                  index === 0 ? acteur : ', ' + acteur
                )}
              </li>
              <li>{item.duree} minutes</li>
            </ul>
            <div className="content" style={{ 'margin-top': '1em' }}>
              <p>{item.synopsys}</p>
            </div>
          </div>
          <div className="card-footer">
            <span className="card-footer-item">
              <a href={item.lien}>Bande annonce</a>
            </span>
            <span className="card-footer-item">{item.seance}</span>
          </div>
        </section>
      </div>
    ))}
  </div>
)

FeatureGrid.propTypes = {
  gridItems: PropTypes.arrayOf(
    PropTypes.shape({
      titre: PropTypes.string,
      image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
      synopsys: PropTypes.string,
      realisateur: PropTypes.string,
      duree: PropTypes.number,
      seance: PropTypes.string,
      lien: PropTypes.string,
      acteurs: PropTypes.arrayOf({
        acteur: PropTypes.string,
      }),
    })
  ),
}

export default FeatureGrid
