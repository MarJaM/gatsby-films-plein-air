import React from 'react'
import { Link } from 'gatsby'
import facebook from '../img/social/facebook.svg'
import instagram from '../img/social/instagram.svg'

const Navbar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      navBarActiveClass: '',
    }
  }

  toggleHamburger = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the navbar accordingly
        this.state.active
          ? this.setState({
              navBarActiveClass: 'is-active',
            })
          : this.setState({
              navBarActiveClass: '',
            })
      }
    )
  }

  render() {
    return (
      <nav
        className="navbar has-background-dark"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container">
          <div
            className={`navbar-burger burger ${this.state.navBarActiveClass}`}
            data-target="navMenu"
            onClick={() => this.toggleHamburger()}
          >
            <span />
            <span />
            <span />
          </div>
          <div
            id="navMenu"
            className={`navbar-menu has-background-dark ${this.state.navBarActiveClass}`}
          >
            <div className="navbar-start has-text-centered ">
              <Link className="navbar-item has-text-weight-semibold" to="/">
                Accueil
              </Link>
              <Link
                className="navbar-item has-text-weight-semibold"
                to="/programme"
              >
                Programme
              </Link>
              <Link
                className="navbar-item has-text-weight-semibold"
                to="/infos-pratiques"
              >
                Infos Pratiques
              </Link>
              <Link
                className="navbar-item has-text-weight-semibold"
                to="/a-propos"
              >
                À propos
              </Link>
              <Link
                className="navbar-item has-text-weight-semibold"
                to="/contact"
              >
                Contact
              </Link>
            </div>
            <div className="navbar-end has-text-centered">
              <a
                className="navbar-item has-background-white"
                href="https://www.facebook.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={facebook} alt="Facebook" />
                </span>
              </a>
              <a
                className="navbar-item has-background-white"
                href="https://www.instagram.com/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span className="icon">
                  <img src={instagram} alt="Instagram" />
                </span>
              </a>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
